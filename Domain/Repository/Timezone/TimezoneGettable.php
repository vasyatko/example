<?php
namespace Example\Domain\Repository\Timezone;

use Example\Domain\Entity\Timezone;

interface TimezoneGettable
{
    /**
     * @param string $city
     * @param string $state
     *
     * @return null|Timezone
     * @throws TimezoneException
     */
    public function getTimezoneForCity(string $city, string $state): ?Timezone;
}
