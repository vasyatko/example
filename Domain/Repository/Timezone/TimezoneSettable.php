<?php
namespace Example\Domain\Repository\Timezone;

use Example\Domain\Entity\Timezone;

interface TimezoneSettable
{
    /**
     * @param Timezone $timezone
     * @return int
     */
    public function addTimezone(Timezone $timezone): int;
}
