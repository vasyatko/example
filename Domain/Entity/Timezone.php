<?php
declare(strict_types=1);

namespace Example\Domain\Entity;

use Example\Domain\ValueObject\Location;
use DateTimeImmutable;

final class Timezone
{
    private const KEY_ID = 'bg_timezone_id';
    private const KEY_CREATED_AT = 'created_at';
    private const KEY_UPDATED_AT = 'updated_at';
    private const KEY_DELETED_AT = 'deleted_at';
    /**
     * @var ?int
     */
    private $id;
    /**
     * @var string
     */
    private $city;
    /**
     * @var string
     */
    private $state;
    /**
     * @var Location
     */
    private $location;
    /**
     * @var string
     */
    private $timezone;
    /**
     * @var null|DateTimeImmutable
     */
    private $createdAt;
    /**
     * @var null|DateTimeImmutable
     */
    private $updatedAt;
    /**
     * @var null|DateTimeImmutable
     */
    private $deletedAt;

    /**
     * Timezone constructor.
     *
     * @param string                 $city
     * @param string                 $state
     * @param Location               $location
     * @param string                 $timezone
     * @param int|null               $id
     * @param DateTimeImmutable|null $createdAt
     * @param DateTimeImmutable|null $updatedAt
     * @param DateTimeImmutable|null $deletedAt
     */
    public function __construct(
        string $city,
        string $state,
        Location $location,
        string $timezone,
        ?int $id = null,
        ?DateTimeImmutable $createdAt = null,
        ?DateTimeImmutable $updatedAt = null,
        ?DateTimeImmutable $deletedAt = null
    ) {
    
        $this->id = $id;
        $this->city = $city;
        $this->state = $state;
        $this->location = $location;
        $this->timezone = $timezone;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->deletedAt = $deletedAt;
    }

    /**
     * @param array $entity
     *
     * @return Timezone
     */
    public static function buildFromArray(array $entity): self
    {
        $location = Location::buildFromParameters((float)$entity['latitude'], (float)$entity['longitude']);
        return new self(
            $entity['city'],
            $entity['state'],
            $location,
            $entity['timezone'],
            (empty($entity[self::KEY_ID])) ? null : $entity[self::KEY_ID],
            (empty($entity[self::KEY_CREATED_AT])) ? null : new DateTimeImmutable($entity[self::KEY_CREATED_AT]),
            (empty($entity[self::KEY_UPDATED_AT])) ? null : new DateTimeImmutable($entity[self::KEY_UPDATED_AT]),
            (empty($entity[self::KEY_DELETED_AT])) ? null : new DateTimeImmutable($entity[self::KEY_DELETED_AT])
        );
    }

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->location->getLongitude();
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->location->getLatitude();
    }

    /**
     * @return Location
     */
    public function getLocation(): Location
    {
        return $this->location;
    }


    /**
     * @return string
     */
    public function getTimezone(): string
    {
        return $this->timezone;
    }

    /**
     * @return null|DateTimeImmutable
     */
    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return null|DateTimeImmutable
     */
    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return null|DateTimeImmutable
     */
    public function getDeletedAt(): ?DateTimeImmutable
    {
        return $this->deletedAt;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::KEY_ID => $this->getId(),
            'city' => $this->getCity(),
            'state' => $this->getState(),
            'latitude' => $this->getLocation()->getLatitude(),
            'longitude' => $this->getLocation()->getLongitude(),
            'timezone' => $this->getTimezone(),
            self::KEY_CREATED_AT => $this->getCreatedAt(),
            self::KEY_UPDATED_AT => $this->getUpdatedAt(),
            self::KEY_DELETED_AT => $this->getDeletedAt(),
        ];
    }
}
