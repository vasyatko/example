<?php
declare(strict_types=1);


namespace Example\Infrastructure\Repository\API\Timezone;

use AnotherExample\Helpers\UrlLoader;
use Example\Domain\Entity\Timezone;
use Example\Domain\Repository\Timezone\TimezoneGettable;
use Example\Domain\Repository\Timezone\TimezoneException;
use Example\Domain\ValueObject\Location;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class GoogleMapsTimezone implements TimezoneGettable
{
    /**
     * @var UrlLoader
     */
    private $urlLoader;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var GoogleMapsApiKeyInterface
     */
    private $apiKeyService;

    /**
     * @param UrlLoader $urlLoader
     * @param Client $client
     * @param GoogleMapsApiKeyInterface
     */
    public function __construct(
        UrlLoader $urlLoader,
        Client $client,
        GoogleMapsApiKeyInterface $apiKeyService
    ) {
        $this->urlLoader     = $urlLoader;
        $this->client        = $client;
        $this->apiKeyService = $apiKeyService;
    }

    /**
     * @param string $city
     * @param string $state
     *
     * @return null|Timezone
     * @throws GoogleMapsApiUnreachableException
     */
    public function getTimezoneForCity(string $city, string $state): ?Timezone
    {
        $key = $this->apiKeyService->getKey();

        $timestamp = time();

        $location = $this->getLocationForCity($city, $state, $key);

        $locationParameter = $location->getLatitude() . ',' . $location->getLongitude();
        $getTimezoneUrl = sprintf(
            $this->urlLoader->url('googlemaps_get_timezone'),
            $locationParameter,
            $key,
            $timestamp
        );
        try {
            $response = $this->client->get($getTimezoneUrl);
        } catch (\Exception $e) {
            throw new GoogleMapsApiUnreachableException();
        }

        $timezoneId = $this->getTimeZoneIdFromResponse($response);
        return new Timezone($city, $state, $location, $timezoneId);
    }

    /**
     * @param string $city
     * @param string $state
     * @param string $key
     *
     * @return Location
     * @throws GoogleMapsApiUnreachableException
     * @throws GoogleMapsApiBadFormattedException
     */
    public function getLocationForCity(string $city, string $state, string $key): Location
    {
        $address = $city . ", " . $state;
        $getLocationUrl = sprintf($this->urlLoader->url('googlemaps_get_location'), $address, $key);
        try {
            $response = $this->client->get($getLocationUrl);
        } catch (\Exception $e) {
            throw new GoogleMapsApiUnreachableException();
        }

        return $this->getLocationFromResponse($response);
    }

    /**
     * @param ResponseInterface $response
     *
     * @return string
     * @throws GoogleMapsApiBadFormattedException
     */
    private function getTimeZoneIdFromResponse(ResponseInterface $response): string
    {
        if ($response->getStatusCode() != 200) {
            throw new GoogleMapsApiBadFormattedException();
        }

        $responseBody = json_decode($response->getBody()->getContents(), true);
        if (!isset($responseBody['status']) ||
            ($responseBody['status'] !== 'OK') ||
            !isset($responseBody['timeZoneId'])) {
            throw new GoogleMapsApiBadFormattedException();
        }
        return (string)$responseBody['timeZoneId'];
    }

    /**
     * @param ResponseInterface $response
     *
     * @return Location
     * @throws GoogleMapsApiBadFormattedException
     */
    private function getLocationFromResponse(ResponseInterface $response): Location
    {
        if ($response->getStatusCode() != 200) {
            throw new GoogleMapsApiBadFormattedException();
        }

        $responseBody = json_decode($response->getBody()->getContents(), true);
        if (!isset($responseBody['results'][0]['geometry']['location'])) {
            throw new GoogleMapsApiBadFormattedException();
        }

        $locationArray = $responseBody['results'][0]['geometry']['location'];
        if (!isset($locationArray['lat']) || !isset($locationArray['lng'])) {
            throw new GoogleMapsApiBadFormattedException();
        }

        return Location::buildFromParameters((float)$locationArray['lat'], (float)$locationArray['lng']);
    }
}
