<?php

namespace Example\Infrastructure\Repository\API\Timezone;

use Example\Domain\Repository\Timezone\TimezoneException;

class GoogleMapsApiUnreachableException extends TimezoneException
{

}
