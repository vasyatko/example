<?php

namespace Example\Infrastructure\Repository\API\Timezone;


use AnotherExample\Helpers\AuthLoader;

class GoogleMapsApiKeyFromConfig implements GoogleMapsApiKeyInterface
{
    /**
     * @var AuthLoader
     */
    private $authLoader;

    /**
     * @param AuthLoader $authLoader
     */
    public function __construct(AuthLoader $authLoader)
    {
        $this->authLoader = $authLoader;
    }

    public function getKey()
    {
        $second = date("s");
        $allKeys = $this->getAllKeysFromConfig();
        $keysAmount = count($allKeys);

        $keyNumberToUse = $second % $keysAmount;

        return $allKeys[$keyNumberToUse];
    }

    /**
     * @return array
     */
    private function getAllKeysFromConfig(): array
    {
        $allKeys = $this->authLoader->auth('googlemapsapi_keys');
        return explode(",", $allKeys);
    }
}
