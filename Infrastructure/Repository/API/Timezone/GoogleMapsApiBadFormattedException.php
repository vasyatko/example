<?php

namespace Example\Infrastructure\Repository\API\Timezone;

use Example\Domain\Repository\Timezone\TimezoneException;

class GoogleMapsApiBadFormattedException extends TimezoneException
{

}
