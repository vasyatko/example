<?php
namespace Example\Infrastructure\Repository\API\Timezone;

interface GoogleMapsApiKeyInterface
{
    public function getKey();
}
